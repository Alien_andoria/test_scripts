require 'fileutils'
require "rubygems"
require "flotr"
require "csv"
require "./Script_Library_IP"
GOODTAG = 0
NOTAG = 1
COLLISION = 2
NBDATA = 2




# Turn off the buffering of the standard output
$stdout.sync = true
puts puts puts
puts '----------------------------------'
puts "	Hello World"
puts "	Test made to receive EPC"
puts "	for 1 tag and determine if"
puts "	we received answer correctly"
puts '----------------------------------'
puts
puts 
if(!ARGV[0])
	puts "Please enter number of Acquisitions in function call argument..."
	puts "Example: >ruby Inventory_loop.rb IP {Nb of acq} 06FM0 {Attenuation} {freq} {mode}"
	puts ""
	puts '----------------------------------'
	exit
else
	_COMPORT = ARGV[0]
end

if(!ARGV[1])
	puts "Please enter number of Acquisitions in function call argument..."
	puts "Example: >ruby Inventory_loop.rb IP {Nb of acq} 06FM0 {Attenuation} {freq} {mode}"
	puts ""
	puts '----------------------------------'
	exit
else
	_NbAcq = ARGV[1].to_i
end

if(!ARGV[2])
	puts "Please enter modulation "
	puts "Example: >ruby Inventory_loop.rb IP {Nb of acq} 06FM0 {Attenuation} {freq} {mode}"
	puts ""
	puts '----------------------------------'
	exit
else
	_Modulation = ARGV[2]
end

if(!ARGV[3])
	puts "Please enter Attenuation "
	puts "Example: >ruby Inventory_loop.rb IP {Nb of acq} 06FM0 {Attenuation} {freq} {mode}"
	puts ""
	puts '----------------------------------'
	exit
else
	_Attenuation = ARGV[3]
end

if(!ARGV[4])
	puts "Please enter Freq "
	puts "Example: >ruby Inventory_loop.rb IP {Nb of acq} 06FM0 {Attenuation} {freq} {mode}"
	puts ""
	puts '----------------------------------'
	exit
else
	_freq = ARGV[4]
end
if(!ARGV[5])
	puts "Please enter Mode "
	puts "Example: >ruby Inventory_loop.rb IP {Nb of acq} 06FM0 {Attenuation} {freq} {mode}"
	puts ""
	puts '----------------------------------'
	exit
else
	if(ARGV[5].to_i <2)
		_TestType = ARGV[5].to_i
	else
		puts "Mode should be 0 or 1"
	end
end

r = AlienReader.new
r.open(_COMPORT)	

path = "FifoResults/"+_Modulation + '/Channel' + _freq

#print "\nSetCommTimeouts result: " + result.to_s + "\n\n"

_firstTag = 0;
#puts GetSysCtrl(serial)
writemod = "function=alien"
puts r.sendreceive(writemod)


writemod = "rfm="+_Modulation
puts r.sendreceive(writemod)


writemod = "freq="+ _freq 
puts r.sendreceive(writemod)



#----------------------------------------------

FileUtils.mkdir_p(path)



_NbGoodTag = 0
_NbCollision = 0
_NbNoTag = 0

puts "-------------------------"
puts " Start of test"
puts "-------------------------"
puts ""
#----------------------------------
#Test Type 0
#Check Rate of GoodTags
#----------------------------------
if(_TestType == 0)
	for i in 1.._NbAcq
		ret = r.sendreceive("rcc = 13 80 06")
		puts ret
		tagfound = ret[6..7].to_i
		puts "TagDetection = " + tagfound.to_s
		if(tagfound == GOODTAG)
			
			_NbGoodTag+=1
		end
		if(tagfound == COLLISION)
			_NbCollision+=1
		end
		if(tagfound == NOTAG)
			_NbNoTag+=1
		end
		
		
	end

	if (_NbGoodTag == _NbAcq)
		puts "-----------------"
		puts "PASS"
	else
		puts "-----------------"
		puts "FAIL"
		puts "Number of CRCError: " + _NbCollision.to_s
		puts "Number of NoTag: " + _NbNoTag.to_s
		puts "% of GoodTag: " + (Float((Float(_NbGoodTag)/Float(_NbAcq))*100)).to_s + "%"
	end
end

if(_TestType == 1)
	puts "Creating test directories..."
	for debug_sel in 4..4
		#Set debug_sel
		SetDebut_sel_IP(r,debug_sel)
		_NbCollision = 0
		_NbGoodTag = 0
		_NbNoTag = 0
		_reads = 0
		while ( ((_NbGoodTag < (NBDATA)) || ( _NbCollision < (NBDATA)) || ( _NbNoTag < (NBDATA))) ) do
			_reads +=1
			puts "--------"
			sleep(0.5)
			r.sendreceive("rcc = 1 db 3")
			ret=r.sendreceive("rcc = 13 80 06")
			puts ret
			tagfound = ret[6..7].to_i
			puts "TagDetection = " + tagfound.to_s
			sysCtrl = GetSysCtrl_IP(r)/4
			puts "SysCtrl= " + sysCtrl.to_s
			if(tagfound == GOODTAG)

				if(_NbGoodTag <NBDATA)
					_NbGoodTag+=1
					fifo1, fifo2 = FifosDumpRam(r)
					if((fifo1 ==0 ) || (fifo2 ==0))
						puts "Skipping tag collection"
						next
					end
					fifo1 = FormatFifoRawRam(fifo1, 1,sysCtrl)
					fifo2 = FormatFifoRawRam(fifo2, 2,sysCtrl)
					if((fifo1 ==0 ) || (fifo2 ==0))
						puts "Skipping formatting"
						next
					end
					filename = path + "/" +_Modulation + "_A=" +_Attenuation+ "_F=" +_freq + "_GOODTAG_" + _NbGoodTag.to_s + "debSel="+ debug_sel.to_s
					FileWriteTxt(filename,"%Modulation: " +_Modulation," Attenuation: " + _Attenuation," Frequency " + _freq + "\n\rfifo1=[",fifo1,"];\r\n\r\nfifo2=[",fifo2, "];")
					ToCSV(fifo1, filename + "_fifo1")
					ToCSV(fifo2, filename + "_fifo2")
					Plotdata(fifo1,1, sysCtrl,filename + "_fifo1" + ".out.csv" )
					Plotdata(fifo2,2, sysCtrl,filename + "_fifo2" + ".out.csv" )
				end

				
			end

			if(tagfound == COLLISION)
=begin
				if(_NbCollision <NBDATA)
					_NbCollision+=1
					fifo1, fifo2 = FifosDumpRam(r)
					if((fifo1 ==0 ) || (fifo2 ==0))
						puts "Skipping tag collection"
						next
					end
					fifo1 = FormatFifoRawRam(fifo1, 1,sysCtrl)
					fifo2 = FormatFifoRawRam(fifo2, 2,sysCtrl)
					if((fifo1 ==0 ) || (fifo2 ==0))
						puts "Skipping formatting"
						next
					end
					filename = path + "/" + _Modulation + "_A=" +_Attenuation+ "_F=" +_freq + "_CRCERROR_" + _NbCollision.to_s + "debSel="+ debug_sel.to_s
					FileWriteTxt(filename,"%Modulation: " +_Modulation," Attenuation: " + _Attenuation," Frequency " + _freq + "\n\rfifo1=[",fifo1,"];\r\n\r\nfifo2=[",fifo2, "];")
					ToCSV(fifo1, filename + "_fifo1")
					ToCSV(fifo2, filename + "_fifo2")
					Plotdata(fifo1,1, sysCtrl,filename + "_fifo1" + ".out.csv" )
					Plotdata(fifo2,2, sysCtrl,filename + "_fifo2" + ".out.csv" )
					name = gets
				end
=end
			end
			if(tagfound == NOTAG)
				if(_NbNoTag<NBDATA)
					_NbNoTag+=1
					fifo1, fifo2 = FifosDumpRam(r)
					if((fifo1 ==0 ) || (fifo2 ==0))
						puts "Skipping tag collection"
						next
					end
					fifo1 = FormatFifoRawRam(fifo1, 1,sysCtrl)
					fifo2 = FormatFifoRawRam(fifo2, 2,sysCtrl)
					if((fifo1 ==0 ) || (fifo2 ==0))
						puts "Skipping formatting"
						next
					end
					filename = path + "/" + _Modulation + "_A=" +_Attenuation+ "_F=" +_freq + "NoTag" + _NbNoTag.to_s + "debSel="+ debug_sel.to_s
					FileWriteTxt(filename,"%Modulation: " +_Modulation," Attenuation: " + _Attenuation," Frequency " + _freq + "\n\rfifo1=[",fifo1,"];\r\n\r\nfifo2=[",fifo2, "];")
					ToCSV(fifo1, filename + "_fifo1")
					ToCSV(fifo2, filename + "_fifo2")
					Plotdata(fifo1,1, sysCtrl,filename + "_fifo1" + ".out.csv" )
					Plotdata(fifo2,2, sysCtrl,filename + "_fifo2" + ".out.csv" )
					name = gets
				end
			end

		end
	end
	puts ""
	puts "RESULT"
	puts "--------------"
	puts "Number of GoodTag: " + _NbGoodTag.to_s
	puts "Number of CRCError: " + _NbCollision.to_s
	puts "Number of NoTag: " + _NbNoTag.to_s
end



