# add the default relative library location to the search path
$:.unshift File.join(File.dirname(__FILE__),'lib')
require 'fileutils'
require 'alienreader'
require 'alienconfig'
require 'io/console'
require 'fcntl'


class Integer
  def to_signed(bits)
    mask = (1 << (bits - 1))
    (self & ~mask) - (self & mask)
  end
end


def SetDebut_sel_IP(r,debug_sel)
	r.sendreceive("rcc = 1 f1 #{(debug_sel>>8).to_s(16)} #{(debug_sel&0xff).to_s(16)}")
end

def FormatRparams(params)
	rParams = "\r\nRxParams: \r\n" + '0:' + params[6..16] + "\r\n"
	rParams += '1: ' + params[18..28] + "\r\n"
	rParams += '2: ' + params[30..40] + "\r\n"
	rParams += '3: ' + params[42..52] + "\r\n"
	rParams += '4: ' + params[54..64] + "\r\n"
	rParams += '5: ' + params[66..76] + "\r\n"
	rParams += '6: ' + params[78..88] + "\r\n"
	rParams += '7: ' + params[90..100] + "\r\n"
	rParams += '8: ' + params[102..112] + "\r\n"
	rParams += '9: ' + params[114..124] + "\r\n"
	rParams += 'FreqErr: ' + params[126..136] + "\r\n"
	
	return rParams
end

def FifosDumpRam(r)
	begin
	
		fifo1 = Array.new
		fifo2 = Array.new
		tstep = 50
		addr = "1E0DE000".to_i(16)
		puts r.sendreceive("rcc = 1 db 4")
		#tstart = Time.now
		count = r.sendreceive("memr = 4 ram 1E0DE000 1")
		count = count[7..14].to_i(16) * 4
		((addr+4)..(addr+count)).step(tstep*4) do |i|
			#puts i.to_s(16)
			if addr+count - i <(tstep*4)
				tstep = (addr+count - i)/4
			end
			#puts i.to_s(16) + ": " + tstep.to_s
			dump = r.sendreceive("memr = 4 ram #{i.to_s(16)} #{tstep.to_s}")
			dump = dump[dump.index('=')+2 .. dump.length]
			dump = dump.split(' ')
			fifo1.push(dump)	
		end
		#puts fifo1
		tstep = 50
		#count = 16384 -1
		count = r.sendreceive("memr = 4 ram 1E0DA000 1")
		count = count[7..14].to_i(16) * 4
		addr = "1E0DA000".to_i(16)
		(addr+4..(addr+count)).step(tstep*4) do |i|		
			if addr+count - i <(tstep*4)
				tstep = (addr+count - i)/4
			end
			#puts i.to_s(16) + ": " + tstep.to_s
			dump = r.sendreceive("memr = 4 ram #{i.to_s(16)} #{tstep.to_s}")
			dump = dump[dump.index('=')+2 .. dump.length]
			dump = dump.split(' ')
			fifo2.push(dump)	
		end
		str_fifo = fifo1.join(";")
		str_fifo1 = '>' + str_fifo + ']'
		str_fifo = fifo2.join(";")
		str_fifo2 = '[' + str_fifo +']'
		return str_fifo1, str_fifo2
	end
		
	
end



def FileWriteTxt(filename,*otherparam)
	f = File.new(filename + ".txt", "w+")
	f.write(otherparam.join())
	f.close
	puts "File saved"
end
def FileWriteMat(filename, fifo1, fifo2)
	fifo1 = fifo1.split(";")
	f = File.new(filename + ".mat", "w+")
	f.write("# name: fifo1 \n")
	f.write("# type: matrix \n")
	f.write("# rows:" + fifo1.length.to_s + "\n")
	f.write("# columns:" + (fifo1[1].count(",") +1).to_s + "\n")
	fifo1.each do |line|
		f.write(" " + line.gsub(",", " ") + "\n")
	end
	fifo2 = fifo2.split(";")
	f.write("# name: fifo2 \n")
	f.write("# type: matrix \n")
	f.write("# rows:" + fifo2.length.to_s + "\n")
	f.write("# columns:" + (fifo2[1].count(",") +1).to_s + "\n")
	fifo2.each do |line|
		f.write(" " + line.gsub(",", " ") + "\n")
	end
	f.close
	
end

def GetSysCtrl_IP(r)
	sysCtrl =r.sendreceive("rcc = 1 f1")
	return sysCtrl[sysCtrl.index('=')+2 .. sysCtrl.index('=')+3].to_i(16)
end

def FormatFifoSigned(fifo, fifonum ,debug_sel)
	if fifonum == 1
		formFifo = fifo[(fifo.index('>')+1) .. (fifo.index(']')-1)]
	else
		formFifo = fifo[(fifo.index('[')+1) .. (fifo.index(']')-1)]	
	end
	return formFifo
end


def FormatFifoRawRam(fifo, fifonum ,sysCtrl)
	begin
		if fifonum == 1
			_fifoArray = []
			
			_fifoArrayFormated = ''
			formFifo = fifo[(fifo.index('>')+1) .. (fifo.index(']')-1)]
			formFifo = formFifo.split(';')
			formFifo.each do |num|
				_fifoArray.push(num.to_i(16))
			end
			_fifoArray.each do |num|
				_fifoArrayFormated += ((((num)>>16)/4).to_signed(14)).to_s + ',' + ((((num>>4)&0x3FFF)).to_signed(14)).to_s + ',' + ((num&0x8)>>3).to_s + ',' + ((num&0x4)>>2).to_s + ',' + ((num&0x2)>>1).to_s + ',' +((num&0x1)).to_s + ';'
				
			end
			return _fifoArrayFormated
		else
			_fifoArray = []
			_fifoArrayFormated = ''
			formFifo = fifo[(fifo.index('[')+1) .. (fifo.index(']')-1)]
			formFifo = formFifo.split(';')
			formFifo.each do |num|
				_fifoArray.push(num.to_i(16))
			end
			_fifoArray.each do |num|
				case ((sysCtrl&0x1c)>>2 )
				when 0	
					_fifoArrayFormated += ((((num)>>16)/4).to_signed(14)).to_s + ',' + ((((num>>4)&0x3FFF)).to_signed(14)).to_s + ',' + ((num&0x8)>>3).to_s + ',' + ((num&0x4)>>2).to_s + ',' + ((num&0x2)>>1).to_s + ',' +((num&0x1)).to_s + ';'
				when 1..3	
					_fifoArrayFormated += (((num)>>16).to_signed(16)).to_s + ',' + ((num&0xFFFF).to_signed(16)).to_s + ';'
				when 4
					_fifoArrayFormated += ((num>>10).to_signed(22)).to_s + ',' + (((num>>8)&0x3).to_signed(2)).to_s + ',' + ((num>>2)&0x1).to_s + ',' + ((num>>1)&0x1).to_s + ',' + ((num)&0x1).to_s + ';'
				else
					puts "sysCtrl Error..."
					puts sysCtrl
					exit
				end
			end
			return _fifoArrayFormated
		end
	rescue
		return 0
	end
end

def ToCSV(fifo,filename)
	ss=fifo.split(";")
	ss.each_with_index do |str, row|
	  str.strip!
	  break if str.empty?
	  File.open(filename + ".out.csv", 'a') { |f| f.write(row.to_s + "," + str + "\n") }
	end
end

def Plotdata(fifo,fifonum, debug_sel, filename)
	plot = Flotr::Plot.new("Fifo #{debug_sel}")
	if(fifonum == 1)
		debug_sel =0
	end
	if(debug_sel ==0)
		# Create empty series
		qChannel = Flotr::Data.new(:label => "Q", :color => "red",)
		iChannel = Flotr::Data.new(:label => "I", :color => "blue",)
		rst = Flotr::Data.new(:label => "rst", :color => "red")
		delimDetect = Flotr::Data.new(:label => "DelimDetect", :color => "purple",)
		sCDetect = Flotr::Data.new(:label => "SCDetect", :color => "green")
		sysClk = Flotr::Data.new(:label => "SysClk", :color => "yellow")

		CSV.foreach(filename) do |row|
			qChannel << [row[0].to_i,row[1].to_i]
			iChannel << [row[0].to_i,row[2].to_i]
			rst << [row[0].to_i,row[6].to_i*20000]
			delimDetect << [row[0].to_i,row[4].to_i*20000]
			sCDetect << [row[0].to_i,row[3].to_i*20000]
			sysClk << [row[0].to_i,row[5].to_i*18000]
		end

		plot << qChannel << iChannel << rst << delimDetect << sCDetect << sysClk
	elsif(debug_sel.between?(1,3))
		# Create empty series
		qChannel = Flotr::Data.new(:label => "Q", :color => "red",)
		iChannel = Flotr::Data.new(:label => "I", :color => "blue",)

		CSV.foreach(filename) do |row|
			qChannel << [row[0].to_i,row[1].to_i]
			iChannel << [row[0].to_i,row[2].to_i]
		end
		plot << qChannel << iChannel
		
	elsif(debug_sel == 4)
		# Create empty series
		qChannel = Flotr::Data.new(:label => "Q", :color => "red",)
		CSV.foreach(filename) do |row|
			qChannel << [row[0].to_i,row[1].to_i]
		end
		plot << qChannel

	end
	
	plot.height = 640
	plot.width = 1500
	plot.save()

	File.rename("././flotr.html", filename + ".html")
end